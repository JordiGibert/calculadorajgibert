package com.UD21.team7.CalculadoraJGibert;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import dto.Operacio;
import visible.CalculadoraVisible;

class Test01 {
	//CalculadoraVisible cv = new CalculadoraVisible();
/*
	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	*/
	@Test
	public void getCuadradaTest() {
		Operacio a = new Operacio(2.0,'2');
		assertEquals(a.getResultat(), 4.0);
		
	}
	
	@Test
	public void getArrelCuadradaTest() {
		Operacio a = new Operacio(4.0,'R');
		assertEquals(a.getResultat(), 2.0);
		
	}
	
	@Test
	public void getSumaTest() {
		Operacio a = new Operacio(4.0, 16.0,'+');
		assertEquals(a.getResultat(), 20.0);
		
	}
	
	@Test
	public void getRestaTest() {
		Operacio a = new Operacio(16.0, 4.0,'-');
		assertEquals(a.getResultat(), 12.0);
		
	}
	
	@Test
	public void getMultiTest() {
		Operacio a = new Operacio(4.0, 16.0,'x');
		assertEquals(a.getResultat(), 64.0);
		
	}
	
	@Test
	public void getDivTest() {
		Operacio a = new Operacio(16.0, 4.0,'/');
		assertEquals(a.getResultat(), 4.0);
		
	}
	
	@Test
	public void getModTest() {
		Operacio a = new Operacio(16.0, 4.0,'%');
		assertEquals(a.getResultat(), 0.0);
		
	}
	
	@Test
	public void getDiv1Test() {
		Operacio a = new Operacio(5.0,'1');
		assertEquals(a.getResultat(), 0.2);
		
	}
}
