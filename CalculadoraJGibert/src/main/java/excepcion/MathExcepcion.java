package excepcion;

public class MathExcepcion extends Exception {
	private int codigoError;
	
    
    public MathExcepcion (int codigoError) {
		super();
		this.codigoError = codigoError;
	}


	@Override
    public String getMessage(){
         
       String mensaje="";
         
        switch(codigoError){
            case 0:
                mensaje="No es posible dividir entre 0";
                break;
            case 1:
                mensaje="No es posible hacer una raiz cuadrada de un numero negativo";
                break;
        }
         
        return mensaje;
         
    }
    
}