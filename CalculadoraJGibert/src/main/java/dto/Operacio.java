package dto;

import excepcion.MathExcepcion;

public class Operacio {
	private double num1;
	private double num2;
	private char operacio;
	public Operacio(double num1, double num2, char operacio) {
		this.num1 = num1;
		this.num2 = num2;
		this.operacio = operacio;
	}
	public Operacio(double num1, char operacio) {
		this.num1 = num1;
		num2=0.0;
		this.operacio = operacio;
	}
	
	public Operacio() {
		this.num1 = 0.0;
		num2=0.0;
		this.operacio = '+';
	}
	
	public String getString() {
		String i = ""+num1+operacio+num2;
		return i;
	}
	
	public Double getResultat() {
		
		double resultat = num1;
		try {
			switch (operacio) {
				case '+':
					resultat=num1+num2;
					break;
				case '-':
					resultat=num1-num2;
					break;
				case 'x':
					resultat=num1*num2;
					break;
				case '/':
					if(num2==0.0) {
						throw new MathExcepcion(0);
					}
					resultat=num1/num2;
					break;
				case '2':
					resultat=num1*num1;
					break;
				case '%':
					resultat=num1%num2;
					break;
				case 'R':
					if(num1<0.0) {
						throw new MathExcepcion(1);
					}
					resultat=Math.sqrt(num1);
					break;
				case '1':
					if(num1==0.0) {
						throw new MathExcepcion(0);
					}
					resultat=1/num1;
					break;
			}
		} catch(MathExcepcion a) {
			System.out.println(a.getMessage());
			resultat=0.0;
		}
		return resultat;
	}
	
	//Geters i setters
	public double getNum1() {
		return num1;
	}
	public double getNum2() {
		return num2;
	}
	public char getOperacio() {
		return operacio;
	}
	public void setNum1(double num1) {
		this.num1 = num1;
	}
	public void setNum2(double num2) {
		this.num2 = num2;
	}
	public void setOperacio(char operacio) {
		this.operacio = operacio;
	}
	
	
}
